package com.gruposca.bimehackday.musicadvisor;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.gruposca.bimehackday.musicadvisor.adapters.ArtistAdapter;
import com.gruposca.bimehackday.musicadvisor.entities.FestivalEntity;
import com.gruposca.bimehackday.musicadvisor.gson.FestivalsGSON;
import com.gruposca.bimehackday.musicadvisor.results.FestivalsResult;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ArtistsActivity extends Activity implements OnItemClickListener
{
	public static final String PARAMETER_ID = "ID";
	public static final String PARAMETER_MODE = "MODE";
	public static final int MODE_IN = 1;
	public static final int MODE_RATE = 2;
	
	private int id;
	private int mode;
	private ListView list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_artists);
		
		//Carga los parametros
		Intent intent = getIntent();
		id = intent.getIntExtra(PARAMETER_ID, 0);
		mode = intent.getIntExtra(PARAMETER_MODE, MODE_IN);
		
		//Carga los festivales
        list = (ListView)findViewById(R.id.list);
        list.setOnItemClickListener(this);
        loadFestival();
        
        //Configure the actionbar
        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch(item.getItemId())
		{
			case android.R.id.home:
	    		finish();
	    		break;
		}
		return false;
	}

	private void loadFestival()
	{
		RequestQueue queue = Volley.newRequestQueue(this);
		FestivalsGSON<FestivalsResult> gsonObjRequest = new FestivalsGSON<FestivalsResult>
		(
			Request.Method.GET, 
			MainActivity.URL, 
			FestivalsResult.class, 
	 		new Response.Listener<FestivalsResult>() 
	 		{  
	            @Override  
	            public void onResponse(FestivalsResult response) 
	            {
	            	for(FestivalEntity entity : response.response.festivals)
	            	{
	            		if (entity.edition_id == id)
	            		{
	            			list.setAdapter(new ArtistAdapter(ArtistsActivity.this, mode == MODE_IN ? R.layout.item_artist_in : R.layout.item_artist_rate, entity.artists));
	            			break;
	            		}
	            	}
	            }
	        },
	        new Response.ErrorListener() 
	        {  
		        @Override  
		        public void onErrorResponse(VolleyError error) 
		        {

		        }  
		    }
	    );
		queue.add(gsonObjRequest);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) 
	{
		if (mode == MODE_RATE)
		{
			Intent intent = new Intent(this, ArtistRateActivity.class);
			startActivity(intent);
		}
	}
}
