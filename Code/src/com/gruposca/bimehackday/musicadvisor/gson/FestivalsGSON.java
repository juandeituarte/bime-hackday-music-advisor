package com.gruposca.bimehackday.musicadvisor.gson;

import java.io.UnsupportedEncodingException;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class FestivalsGSON<t> extends Request<t>
{  
	private Gson mGson;
	private Class<t> mClass;
	private Listener<t> mListener;
	
	public FestivalsGSON(int method, String url, Class<t> clazz, Listener<t> listener, ErrorListener errorListener) 
	{
	      super(method, url, errorListener);
	      mGson = new GsonBuilder()
		      .setDateFormat("yyyy-MM-dd HH:mm:ss")
		      .create();
	      mClass = clazz;
	      mListener = listener;
	      Log.d("Beem", "GsonRequest");
	}
	  
	@Override  
	protected Response<t> parseNetworkResponse(NetworkResponse response) 
	{  
		try 
		{
			String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers)); 
			t parsedGSON = mGson.fromJson(jsonString, mClass);  	      
			return Response.success(parsedGSON,HttpHeaderParser.parseCacheHeaders(response));
		}
		catch (UnsupportedEncodingException e) 
		{  
			return Response.error(new ParseError(e));  
		} 
		catch (JsonSyntaxException je) 
		{  
			return Response.error(new ParseError(je));  
		}  
	}

	@Override
	protected void deliverResponse(t response) 
	{
		mListener.onResponse(response);
	}
} 
