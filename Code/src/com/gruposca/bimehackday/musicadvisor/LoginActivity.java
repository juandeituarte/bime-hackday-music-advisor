package com.gruposca.bimehackday.musicadvisor;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
//import android.view.Menu;
import android.content.Intent;

public class LoginActivity extends Activity 
{
	Intent siguiente;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		siguiente = new Intent(this, MainActivity.class);
	}

	public void onLogin(View view)
	{
		startActivityForResult(siguiente, 0);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		finish();
	}
}

