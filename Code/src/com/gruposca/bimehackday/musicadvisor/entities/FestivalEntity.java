package com.gruposca.bimehackday.musicadvisor.entities;

import java.util.Date;
import java.util.List;

public class FestivalEntity 
{	
	public int edition_id;
	public String name;
	public List<ArtistEntity> artists;
	public Date startDate;
	public Date endDate;
	public String description;
	public ImagesEntity images;
	public String url;
}