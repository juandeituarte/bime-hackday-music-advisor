package com.gruposca.bimehackday.musicadvisor.entities;

public class BadgeEntity
{
	public int icon;
	public int title;
	public int color;
	
	public BadgeEntity(int icon, int title, int color)
	{
		this.icon = icon;
		this.title = title;
		this.color = color;
	}
}
