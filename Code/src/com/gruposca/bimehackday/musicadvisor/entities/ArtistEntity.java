package com.gruposca.bimehackday.musicadvisor.entities;

import java.util.Random;

import com.gruposca.bimehackday.musicadvisor.R;

public class ArtistEntity 
{
	public int id;
	public String url;
	public String name;
	
	private Integer rateResource = null;
	public int getRateResource()
	{
		if (rateResource == null)
		{
			switch(new Random().nextInt(5))
			{
				case 0:
					rateResource = R.drawable.action_rate1;
					break;
				case 1:
					rateResource = R.drawable.action_rate2;
					break;
				case 2:
					rateResource = R.drawable.action_rate3;
					break;
				case 3:
					rateResource = R.drawable.action_rate4;
					break;
				case 4:
					rateResource = R.drawable.action_rate5;
					break;
			}
		}
		return rateResource;
	}
	
	private Boolean onFire = null;
	public Boolean isOnFire()
	{
		if (onFire == null)
			onFire = new Random().nextBoolean();
		return onFire;
	}
}