package com.gruposca.bimehackday.musicadvisor.entities;

public class MenuEntity 
{
	public int icon;
	public int title;
	
	public MenuEntity(int icon, int title)
	{
		this.icon = icon;
		this.title = title;
	}
}