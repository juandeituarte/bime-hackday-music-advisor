package com.gruposca.bimehackday.musicadvisor;

import org.jsoup.Jsoup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.gruposca.bimehackday.musicadvisor.adapters.FestivalAdapter;
import com.gruposca.bimehackday.musicadvisor.entities.FestivalEntity;
import com.gruposca.bimehackday.musicadvisor.gson.FestivalsGSON;
import com.gruposca.bimehackday.musicadvisor.results.FestivalsResult;
import com.gruposca.bimehackday.musicadvisor.tools.VolleySingleton;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FestivalActivity extends Activity 
{
	private Intent siguiente,siguientealt;
	int id;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_festival);
		siguiente = new Intent(this,ArtistsActivity.class);
		siguientealt = new Intent(this,ArtistsActivity.class);
		//coger parametros pasados desde main
		Bundle extra = this.getIntent().getExtras();
		
		//carga imagen via http
		com.android.volley.toolbox.NetworkImageView icon = (com.android.volley.toolbox.NetworkImageView)findViewById(R.id.icon);
		String url = extra.getString("Icon");
			
		icon.setDefaultImageResId(R.drawable.no_picture_large);
		icon.setErrorImageResId(R.drawable.no_picture_large);
		 
		if (url != null && !url.isEmpty()){
		 icon.setImageUrl(url, VolleySingleton.getInstance(getBaseContext()).getImageLoader());
		}
		
		/*if (url==null || url.isEmpty()){
			 icon.setImageUrl("http://fortin.c.free.fr/public/no_picture_decorator.png", VolleySingleton.getInstance(getBaseContext()).getImageLoader());
		 }else{
			 icon.setImageUrl(url, VolleySingleton.getInstance(getBaseContext()).getImageLoader());
     	 }*/
				
		//texto evento
		TextView tv = (TextView)findViewById(R.id.name);
		tv.setText(extra.getString("Name"));
		
		String description = getIntent().getExtras().getString("Description");		
		((TextView)findViewById(R.id.description)).setText(Jsoup.parse(Html.fromHtml(description).toString()).text());	
		
		
		id = extra.getInt("ID");
		
		//Configure the actionbar
        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch(item.getItemId())
		{
			case android.R.id.home:
	    		finish();
	    		break;
		}
		return false;
	}
	
	public void onIn(View view){
		siguiente.putExtra("ID",id);
		siguiente.putExtra("MODE", 1);
		startActivity(siguiente);
	}
	public void onRate(View view){
		siguientealt.putExtra("ID",id);
		siguientealt.putExtra("MODE", 2);
		startActivity(siguientealt);
		}
}