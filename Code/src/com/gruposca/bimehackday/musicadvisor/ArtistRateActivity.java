package com.gruposca.bimehackday.musicadvisor;


import com.android.volley.toolbox.NetworkImageView;
import com.gruposca.bimehackday.musicadvisor.tools.VolleySingleton;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ArtistRateActivity extends Activity {

	public static final String NAME = "Supersubmarina";	
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_artist_rate);		
		((TextView)findViewById(R.id.name)).setText(NAME);		
		ImageView icon= (ImageView)findViewById(R.id.icon);		
		icon.setImageResource(R.drawable.image_supersubmarina);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.artist_rate, menu);
		return true;
	}

}
