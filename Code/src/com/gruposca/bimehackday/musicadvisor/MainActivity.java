package com.gruposca.bimehackday.musicadvisor;

import java.util.ArrayList;
import java.util.List;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.gruposca.bimehackday.musicadvisor.adapters.FestivalAdapter;
import com.gruposca.bimehackday.musicadvisor.adapters.MenuAdapter;
import com.gruposca.bimehackday.musicadvisor.entities.FestivalEntity;
import com.gruposca.bimehackday.musicadvisor.entities.MenuEntity;
import com.gruposca.bimehackday.musicadvisor.gson.FestivalsGSON;
import com.gruposca.bimehackday.musicadvisor.results.FestivalsResult;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;

public class MainActivity extends Activity implements OnItemClickListener
{
	//public static final String URL = "http://fortin.c.free.fr/public/json.txt";
	public static final String URL = "http://www.nvivo.es/api/request.php?api_key=3f1a7bb075b0a69f8a19b6780731ad6e&method=festival.getFestivalsByCountry&country_iso=3166&format=json";
	
	private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private ListView list;
    private ProgressBar progress;
    private Intent siguiente;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		siguiente = new Intent(this, FestivalActivity.class);
		
		//Add the drawer
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) 
        {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) 
            {

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) 
            {

            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        
        //Add the list menu
        List<MenuEntity> items = new ArrayList<MenuEntity>();
        items.add(new MenuEntity(R.drawable.action_festivals, R.string.title_activity_main));
        items.add(new MenuEntity(R.drawable.action_badges, R.string.title_activity_badges));
        drawerList = (ListView)findViewById(R.id.left_drawer);
        drawerList.setAdapter(new MenuAdapter(this, items));
        drawerList.setOnItemClickListener(new OnItemClickListener() 
        {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
			{
				selectItem(position);
			}
        });
        drawerList.setSelector(R.drawable.menu_selector);
        
        //Carga los festivales
        list = (ListView)findViewById(R.id.festivalList);
        list.setOnItemClickListener(this);
        progress = (ProgressBar)findViewById(R.id.progress);
        loadFestivals();
        
        
        //Configure the actionbar
        ActionBar bar = getActionBar();
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
	}
	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	*/
	@Override
    protected void onPostCreate(Bundle savedInstanceState) 
    {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
	{
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (drawerToggle.onOptionsItemSelected(item)) 
        {
        	return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
	
	private void selectItem(int position) 
    {
        drawerList.setItemChecked(position, true);
        drawerLayout.closeDrawer(drawerList);
        
        switch(position)
        {
	        case 0:
	        	break;
	        	
	        case 1:
	        	Intent badges = new Intent(this, BadgesActivity.class);
	        	startActivity(badges);
	        	break;
        }
    }
	
	private void loadFestivals()
	{
		RequestQueue queue = Volley.newRequestQueue(this);
		FestivalsGSON<FestivalsResult> gsonObjRequest = new FestivalsGSON<FestivalsResult>
		(
			Request.Method.GET, 
			URL, 
			FestivalsResult.class, 
	 		new Response.Listener<FestivalsResult>() 
	 		{  
	            @Override  
	            public void onResponse(FestivalsResult response) 
	            {
	            	list.setAdapter(new FestivalAdapter(MainActivity.this, response.response.festivals));
	            	progress.setVisibility(View.GONE);
	            	list.setVisibility(View.VISIBLE);
	            }
	        },
	        new Response.ErrorListener() 
	        {  
		        @Override  
		        public void onErrorResponse(VolleyError error) 
		        {

		        }  
		    }
	    );
		queue.add(gsonObjRequest);
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		FestivalEntity item = (FestivalEntity) arg0.getItemAtPosition(arg2);
		siguiente.putExtra("Name", item.name);
		siguiente.putExtra("Icon", item.images.large);
		siguiente.putExtra("ID", item.edition_id);
		siguiente.putExtra("Description", item.description);
		startActivity(siguiente);
	}
	
}