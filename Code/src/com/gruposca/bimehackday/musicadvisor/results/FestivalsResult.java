package com.gruposca.bimehackday.musicadvisor.results;

import java.util.List;

import com.gruposca.bimehackday.musicadvisor.entities.FestivalEntity;

public class FestivalsResult 
{
	public String status;
	public FestivalResponse response;

	public class FestivalResponse
	{
		public List<FestivalEntity> festivals;
	}
}
