package com.gruposca.bimehackday.musicadvisor.dialogs;

import com.gruposca.bimehackday.musicadvisor.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

public class CheckinDialog extends DialogFragment 
{
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) 
	{
		Activity activity = getActivity();
		View view = (View)activity.getLayoutInflater().inflate(R.layout.dialog_checkin, null);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Te tenemos!!!")
			.setView(view)
        	.setInverseBackgroundForced(true)
        	.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
    		{
    			public void onClick(DialogInterface dialog, int id) 
    			{
    				dialog.cancel();
    			}
    		});
		
        return builder.create();
    }
}
