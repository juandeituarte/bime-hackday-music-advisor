package com.gruposca.bimehackday.musicadvisor;

import java.util.ArrayList;
import java.util.List;

import com.gruposca.bimehackday.musicadvisor.adapters.BadgeAdapter;
import com.gruposca.bimehackday.musicadvisor.entities.BadgeEntity;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.content.res.Resources;
import android.view.MenuItem;
import android.widget.GridView;

public class BadgesActivity extends Activity 
{
	private GridView grid;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_badges);
		
		//Carga las medallas
		Resources resources = getResources();
		List<BadgeEntity> items = new ArrayList<BadgeEntity>();
        items.add(new BadgeEntity(R.drawable.badge_1, R.string.title_activity_main, resources.getColor(R.color.blue)));
        items.add(new BadgeEntity(R.drawable.badge_2, R.string.title_activity_badges, resources.getColor(R.color.red)));
        items.add(new BadgeEntity(R.drawable.badge_3, R.string.title_activity_main, resources.getColor(R.color.blue)));
        items.add(new BadgeEntity(R.drawable.badge_4, R.string.title_activity_badges, resources.getColor(R.color.red)));
        items.add(new BadgeEntity(R.drawable.badge_5, R.string.title_activity_main, resources.getColor(R.color.blue)));
		grid = (GridView)findViewById(R.id.grid);
		grid.setAdapter(new BadgeAdapter(this, items));

		//Configura el actionbar
		ActionBar bar = getActionBar();
		bar.setLogo(R.drawable.action_badges);
		bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) 
	{
		switch(item.getItemId())
		{
			case android.R.id.home:
	    		finish();
	    		break;
		}
		return false;
	}
}