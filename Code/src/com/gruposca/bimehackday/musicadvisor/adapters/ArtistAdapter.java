package com.gruposca.bimehackday.musicadvisor.adapters;

import java.util.List;

import com.gruposca.bimehackday.musicadvisor.R;
import com.gruposca.bimehackday.musicadvisor.dialogs.CheckinDialog;
import com.gruposca.bimehackday.musicadvisor.entities.ArtistEntity;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ArtistAdapter extends ArrayAdapter<ArtistEntity>
{
	private Activity activity;
	private int resourceId;
	
	public ArtistAdapter(Activity context, int resourceId, List<ArtistEntity> objects) 
	{
		super(context, resourceId, objects);
		this.activity = context;
		this.resourceId = resourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		 View view = convertView;
		 
		 if (view == null) 
		 {            
			 LayoutInflater li = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 view = li.inflate(resourceId, null); 
		 } 
		 ArtistEntity item = getItem(position);
		 if (item != null)
		 {
			 TextView tv = (TextView)view.findViewById(R.id.text);
			 tv.setText(item.name);
			 
			 if (resourceId == R.layout.item_artist_in)
			 {
				 if (item.isOnFire())
					 view.findViewById(R.id.fire).setVisibility(View.VISIBLE);
				 else
					 view.findViewById(R.id.fire).setVisibility(View.INVISIBLE);
				 
				 ImageView rate = (ImageView)view.findViewById(R.id.rate);
				 rate.setImageResource(item.getRateResource());
				 
				 ImageView checkin = (ImageView)view.findViewById(R.id.checkin);
				 
				
				 checkin.setOnClickListener(new OnClickListener()
				 {
					@Override
					public void onClick(View v) 
					{
						new CheckinDialog().show(activity.getFragmentManager(), "Dialog");
					}
				 });
			 }
			 else
			 {
				 
			 }
		 }

	     return view;
	}
}
