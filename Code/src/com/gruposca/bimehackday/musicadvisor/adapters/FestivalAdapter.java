package com.gruposca.bimehackday.musicadvisor.adapters;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import org.jsoup.*;

import com.gruposca.bimehackday.musicadvisor.R;
import com.gruposca.bimehackday.musicadvisor.entities.FestivalEntity;
import com.gruposca.bimehackday.musicadvisor.tools.VolleySingleton;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;


public class FestivalAdapter extends ArrayAdapter<FestivalEntity>
{
	public FestivalAdapter(Context context, List<FestivalEntity> objects) 
	{
		super(context, 0, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		 View view = convertView;
		 Format formatter;
		 String fecha;
		 NetworkImageView icon;
		 
		 if (view == null) 
		 {            
			 LayoutInflater li = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 view = li.inflate(R.layout.item_festival, null); 
		 } 
		 FestivalEntity item = getItem(position);
		 if (item != null)
		 {
			 TextView nameView = (TextView)view.findViewById(R.id.row_name); 
			 nameView.setText(item.name);
			 
			 icon = (com.android.volley.toolbox.NetworkImageView)view.findViewById(R.id.row_icon);
			 icon.setDefaultImageResId(R.drawable.no_picture_large);
			 icon.setErrorImageResId(R.drawable.no_picture_large);
			 
			 if (item.images.large != null && !item.images.large.isEmpty()){
				 icon.setImageUrl(item.images.large, VolleySingleton.getInstance(getContext()).getImageLoader());
			 }
			 
			 formatter = new SimpleDateFormat("dd-MM-yy");
			 fecha = formatter.format(item.startDate);
			 TextView fechaView = (TextView)view.findViewById(R.id.row_date); 
			 fechaView.setText(fecha);
			 
			 //((TextView)view.findViewById(R.id.row_description)).setText(Html.fromHtml(item.description));
			 ((TextView)view.findViewById(R.id.row_description)).setText(Jsoup.parse(Html.fromHtml(item.description).toString()).text());
		 }
	        
	     return view;
	}
}
