package com.gruposca.bimehackday.musicadvisor.adapters;

import java.util.List;

import com.gruposca.bimehackday.musicadvisor.R;
import com.gruposca.bimehackday.musicadvisor.entities.MenuEntity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MenuAdapter extends ArrayAdapter<MenuEntity>
{
	public MenuAdapter(Context context, List<MenuEntity> objects) 
	{
		super(context, 0, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		 View view = convertView;
	     
		 if (view == null) 
		 {            
			 LayoutInflater li = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 view = li.inflate(R.layout.item_menu, null); 
		 } 
		 MenuEntity item = getItem(position);
		 if (item != null)
		 {
			 TextView tvType = (TextView)view.findViewById(R.id.row_entry); 
			 tvType.setText(item.title);
			 tvType.setCompoundDrawablesWithIntrinsicBounds(item.icon, 0, 0, 0);
		 }
	        
	     return view;
	}
}
