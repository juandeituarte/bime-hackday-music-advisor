package com.gruposca.bimehackday.musicadvisor.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gruposca.bimehackday.musicadvisor.R;
import com.gruposca.bimehackday.musicadvisor.entities.BadgeEntity;

public class BadgeAdapter extends ArrayAdapter<BadgeEntity>
{
	public BadgeAdapter(Context context, List<BadgeEntity> objects) 
	{
		super(context, 0, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		 View view = convertView;
	     
		 if (view == null) 
		 {            
			 LayoutInflater li = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			 view = li.inflate(R.layout.item_badge, null); 
		 } 
		 BadgeEntity item = getItem(position);
		 if (item != null)
		 {
			 ((ImageView)view.findViewById(R.id.row_entry)).setImageResource(item.icon); 
			 
		 }
	        
	     return view;
	}
}
